package com.example.beanfact;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BeanFactory fact=new XmlBeanFactory(new ClassPathResource("beans.xml"));
        Department dpt=(Department) fact.getBean("dept2");
        
        System.out.println(dpt.nameToUpper());
       //dpt.nameToUpper();
    }
}
