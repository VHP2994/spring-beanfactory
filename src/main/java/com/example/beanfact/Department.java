package com.example.beanfact;

public class Department {

	private Employee employee;
	
	
	
	public Department(Employee employee) {
		this.employee=employee;
	}
	
	public String nameToUpper() {
		return employee.getEmpname().toUpperCase();
	}
}
